import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import { BsQuestionCircle } from 'react-icons/bs';
import { FiUser } from 'react-icons/fi';
import { CiLogin } from 'react-icons/ci';
import { FaRegUserCircle } from 'react-icons/fa';
import { AiOutlineAlignLeft, AiOutlineAlignRight } from 'react-icons/ai';

const Header = ({ setCollapsed, collapsed }) => {
    return (
        <div className="main w-11/12 mx-9 mt-4">
            <nav className="h-16 flex justify-between items-center">
                <div className="flex items-center justify-items-center">
                    {React.createElement(collapsed ? AiOutlineAlignLeft : AiOutlineAlignRight, {
                        className: 'text-2xl cursor-pointer',
                        onClick: () => setCollapsed(!collapsed),
                    })}
                    <h3 className="pl-4 font-bold text-lg max-lg:hidden">Home</h3>
                </div>
                <div className="flex items-center justify-items-start">
                    <Link to="/billing">
                        <button className="btn btn-outline btn-warning btn-sm mr-5 font-normal">Warning</button>
                    </Link>
                    <div className="dropdown dropdown-end flex">
                        <div className="tooltip tooltip-bottom w-full" data-tip="Help">
                            <BsQuestionCircle tabIndex={0} className="text-xl m-1 mr-3 cursor-pointer outline-none" />
                        </div>
                        <ul
                            tabIndex={0}
                            className="dropdown-content menu p-2 shadow rounded-md w-48 z-10 bg-white mt-5 top-8 "
                        >
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    Help Center
                                </Link>
                            </li>
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    Contact Support
                                </Link>
                            </li>
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    Plans and Pricing
                                </Link>
                            </li>
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    Term and policy
                                </Link>
                            </li>
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">About Us</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="dropdown dropdown-end">
                        <FaRegUserCircle tabIndex={0} className="text-xl m-1 mr-3 cursor-pointer" />
                        <ul
                            tabIndex={0}
                            className="dropdown-content menu p-2 shadow rounded-md w-48 z-10 bg-white mt-5 "
                        >
                            <li>
                                <Link className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    <FiUser />
                                    Profile
                                </Link>
                            </li>
                            <li>
                                <Link to="/" className="text-gray-700 hover:text-red-500 bg-transparent h-11">
                                    <CiLogin /> Log out
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <span className="pl-3 max-lg:hidden">Vanlinh</span>
                </div>
            </nav>
            <Outlet />
        </div>
    );
};

export default Header;
