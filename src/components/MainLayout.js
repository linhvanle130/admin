import React, { useState } from 'react';
import images from '../images';
import { Link } from 'react-router-dom';
import { AiOutlineHome, AiOutlineFileText } from 'react-icons/ai';
import { MdOutlinePayment } from 'react-icons/md';
import { FiUsers } from 'react-icons/fi';
import { BsLink45Deg, BsQuestionCircle, BsXLg } from 'react-icons/bs';
import { BiExpandHorizontal } from 'react-icons/bi';
import { TfiMedallAlt } from 'react-icons/tfi';
import Header from './header';

const MainLayout = () => {
    const [collapsed, setCollapsed] = useState(false);
    const [colorOverview, setColorOverview] = useState(true);

    return (
        <div className="w-full flex flex-row min-h-screen">
            <div
                className={collapsed ? 'w-20 rounded-tr-3xl  ' : 'w-64  rounded-tr-3xl max-sm:hidden max-lg:w-20'}
                style={{ background: '#16213e' }}
            >
                <div className="logo flex items-center justify-between relative p-4">
                    <div className="flex justify-items-center">
                        <img src={images.logo} alt="logo" />
                        {collapsed ? '' : <h3 className="pl-2 text-white w-14 max-lg:hidden">Ecomdy Media</h3>}
                    </div>
                    {collapsed ? (
                        ''
                    ) : (
                        <BsXLg
                            className="text-xl text-red-600 cursor-pointer max-lg:hidden"
                            onClick={() => setCollapsed(!collapsed)}
                        />
                    )}
                </div>
                <div className="relative">
                    <ul className="absolute w-full">
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Overview">
                                <Link to="/home" onClick={(e) => setColorOverview(!colorOverview)}>
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <AiOutlineHome
                                            className={colorOverview ? 'text-xl text-red-400' : 'text-xl text-white'}
                                        />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Overview</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Billing">
                                <Link to="/billing">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <AiOutlineFileText className="text-xl " />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Billing</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="My ad accounts">
                                <Link to="/my-accounts">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <FiUsers className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">My ad accounts</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Payment">
                                <Link to="/payment">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0 '
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <MdOutlinePayment className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Payment</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Product Links">
                                <Link to="/product">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <BsLink45Deg className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Product Links</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Pixel Management">
                                <Link to="/payment">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <BiExpandHorizontal className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Pixel Management</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="Referral Program">
                                <Link to="/referral-program">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <TfiMedallAlt className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">Referral Program</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                        <li className="my-2">
                            <div className="tooltip tooltip-right  w-full" data-tip="FAQs">
                                <Link to="/fAQs">
                                    <div
                                        className={
                                            !collapsed
                                                ? 'flex items-center justify-items-center py-2 mx-4 my-1 text-white hover:pl-2 hover:bg-slate-600 hover:rounded-xl cursor-pointer max-lg:justify-center max-lg:hover:pl-0'
                                                : 'flex items-center justify-center py-2 my-1 mx-4 text-white  hover:bg-slate-600 hover:rounded-xl cursor-pointer mx-4:hidden'
                                        }
                                    >
                                        <BsQuestionCircle className="text-xl" />
                                        {collapsed ? '' : <p className="pl-3 max-lg:hidden">FAQs</p>}
                                    </div>
                                </Link>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <Header setCollapsed={setCollapsed} collapsed={collapsed} heading />
        </div>
    );
};

export default MainLayout;
