import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { MainLayout } from './components';
import { Login, Register, ForgotPassword, ResetPassword } from './pages/Auth';
import Home from './pages/home';
import Billing from './pages/billing';
import MyAccount from './pages/my-accounts';
import Payment from './pages/payment';
import Product from './pages/product';
import PixelManagement from './pages/pixel-management';
import ReferralProgram from './pages/referral-program';
import FAQs from './pages/faqs';

import Profile from './pages/profile';

function App() {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/forgot-password" element={<ForgotPassword />} />
                <Route path="/reset-password" element={<ResetPassword />} />
                <Route path="/" element={<MainLayout />}>
                    <Route path="/home" element={<Home />} />
                    <Route path="/billing" index element={<Billing />} />
                    <Route path="/my-accounts" index element={<MyAccount />} />
                    <Route path="/payment" index element={<Payment />} />
                    <Route path="/product" index element={<Product />} />
                    <Route path="/pixel-management" index element={<PixelManagement />} />
                    <Route path="/referral-program" element={<ReferralProgram />} />
                    <Route path="/fAQs" index element={<FAQs />} />

                    <Route path="/profile" index element={<Profile />} />
                </Route>
            </Routes>
        </Router>
    );
}

export default App;
